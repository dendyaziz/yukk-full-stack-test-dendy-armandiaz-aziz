const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [
    // prettier-ignore
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      borderColor: theme => ({
        DEFAULT: theme('colors.gray.200', 'currentColor'),
      }),
      fontFamily: {
        sans: ['Cerebri Sans', ...defaultTheme.fontFamily.sans],
      },
      boxShadow: theme => ({
        outline: '0 0 0 2px ' + theme('colors.indigo.500'),
      }),
      fill: theme => theme('colors'),
    },
  },
  variants: {
    extend: {
      fill: ['focus', 'group-hover'],
    },
  },

  daisyui: {
    themes: [
      {
        mytheme: {
          'primary': '#FF6C14',
          'secondary': '#FFBE00',
          'accent': '#333D4D',

          'neutral': '#333D4D',
          'base-100': '#FFFFFF',
          'info': '#00B6FF',
          'success': '#00AA6E',
          'warning': '#FFBE00',
          'error': '#FF5861',

          '--input-size-xs': '2rem',
          '--input-size-sm': '2.5rem',
          '--input-size-md': '3rem',
          '--input-size-lg': '3.5rem',

          '.navbar': {
            'min-height': 'var(--input-size-lg)',
          },
          '.btn,.btn-md': {
            'height': 'var(--input-size-md)',
            'min-height': 'var(--input-size-md)',
          },
          '.btn-xs': {
            'height': 'var(--input-size-xs)',
            'min-height': 'var(--input-size-xs)',
          },
          '.btn-sm': {
            'height': 'var(--input-size-sm)',
            'min-height': 'var(--input-size-sm)',
          },
          '.btn-lg': {
            'height': 'var(--input-size-lg)',
            'min-height': 'var(--input-size-lg)',
          },
          '.btn-square,.btn-circle': {
            width: 'var(--input-size-md)',
          },
          '.btn-square.btn-xs,.btn-circle.btn-xs': {
            width: 'var(--input-size-xs)',
          },
          '.btn-square.btn-sm,.btn-circle.btn-sm': {
            width: 'var(--input-size-sm)',
          },
          '.btn-square.btn-lg,.btn-circle.btn-lg': {
            width: 'var(--input-size-lg)',
          },
          '.file-input,.select,.input': {
            height: 'var(--input-size-md)',
          },
          '.file-input.file-input-xs,.select.select-xs,.input.input-xs': {
            height: 'var(--input-size-xs)',
          },
          '.file-input.file-input-sm,.select.select-sm,.input.input-sm': {
            height: 'var(--input-size-sm)',
          },
          '.file-input.file-input-lg,.select.select-lg,.input.input-lg': {
            height: 'var(--input-size-lg)',
          },
        },
      },
    ],
  },
  plugins: [require("daisyui")],
}
