<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\OrganizationsController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\SendController;
use App\Http\Controllers\TopupController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth

Route::get('login', [AuthenticatedSessionController::class, 'create'])
    ->name('login')
    ->middleware('guest');

Route::post('login', [AuthenticatedSessionController::class, 'store'])
    ->name('login.store')
    ->middleware('guest');

Route::get('register', [RegisterController::class, 'create'])
    ->name('register')
    ->middleware('guest');

Route::post('register', [RegisterController::class, 'store'])
    ->name('register.store')
    ->middleware('guest');

Route::delete('logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');


// Home

Route::get('/', [HomeController::class, 'index'])
    ->name('home')
    ->middleware('auth');


// Transaction

Route::get('history', [TransactionController::class, 'index'])
    ->name('transactions')
    ->middleware('auth');

Route::get('topup', [TopupController::class, 'topup'])
    ->name('topup')
    ->middleware('auth');

Route::get('topup/{transaction_code}', [TopupController::class, 'payment'])
    ->name('topup.payment')
    ->middleware('auth');

Route::post('topup/{transaction_code}/verify', [TopupController::class, 'verify'])
    ->name('topup.verify')
    ->middleware('auth');

Route::post('topup', [TopupController::class, 'store'])
    ->name('topup.store')
    ->middleware('auth');

Route::get('send', [SendController::class, 'send'])
    ->name('send')
    ->middleware('auth');

Route::post('send', [SendController::class, 'store'])
    ->name('send.store')
    ->middleware('auth');
