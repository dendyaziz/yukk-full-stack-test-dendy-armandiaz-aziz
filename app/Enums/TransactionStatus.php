<?php

namespace App\Enums;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TransactionStatus
{
    const PENDING = 'pending';
    const SUCCESS = 'success';
}
