<?php

namespace App\Http\Controllers;

use App\Enums\TransactionStatus;
use App\Models\Topup;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class TopupController extends Controller
{
    public function topup()
    {
        return Inertia::render('Transaction/Topup');
    }

    public function payment($transactionCode)
    {
        $transaction = Transaction::where('code', $transactionCode)->firstOrFail();

        return Inertia::render('Transaction/TopupPayment', [
            'transaction' => $transaction,
        ]);
    }

    public function verify($transactionCode)
    {
        Request::validate([
            'file' => ['required', 'file', 'image', 'max:2048'],
        ]);

        try {
            DB::beginTransaction();
            $transaction = Transaction::where('code', $transactionCode)->firstOrFail();

            $filename = $transaction->code . '.' . Request::file('file')->getClientOriginalExtension();
            $filepath = "payment_receipt/$filename";

            Request::file('file')->move('payment_receipt', $filename);

            $transaction->status = TransactionStatus::SUCCESS;
            $transaction->receipt_path = $filepath;
            $transaction->save();

            Auth::user()->increment('balance', $transaction->amount);

            DB::commit();
            return Redirect::route('home')->with('success', 'Topup successful!');
        } catch (\Exception $error) {
            DB::rollBack();
            return Redirect::route('home')->with('error', 'Oops, something went wrong!');
        }
    }

    public function store()
    {
        Request::validate([
            'amount' => ['required', 'integer', 'min:10000', 'max:2000000'],
        ]);

        try {
            DB::beginTransaction();

            $topup = Topup::create();

            $transaction = new Transaction();

            $transaction->user_id = auth()->id();
            $transaction->amount = Request::get('amount');
            $transaction->code = 'TU' . time();
            $transaction->status = TransactionStatus::PENDING;

            $transaction->transactionable()->associate($topup);
            $transaction->save();

            DB::commit();
            return Redirect::to("topup/$transaction->code");
        } catch (\Exception $error) {
            DB::rollBack();
            return Redirect::route('home')->with('error', 'Oops, something went wrong!');
        }
    }
}
