<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        return Inertia::render('Home/Index', [
            'transactions' => Auth::user()->transactions()
                ->with('transactionable')
                ->orderBy('created_at', 'desc')
                ->paginate(3)
                ->through(fn ($organization) => [
                    'id' => $organization->id,
                    'code' => $organization->code,
                    'amount' => $organization->amount,
                    'status' => $organization->status,
                    'description' => $organization->description,
                    'receipt_path' => $organization->receipt_path,
                    'type' => $organization->transactionable_type,
                    'transactionable' => $organization->transactionable,
                    'created_at' => $organization->created_at,
                ]),
        ]);
    }
}
