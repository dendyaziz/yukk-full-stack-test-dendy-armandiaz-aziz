<?php

namespace App\Http\Controllers;

use App\Enums\TransactionStatus;
use App\Models\Transaction;
use App\Models\Transfer;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class SendController extends Controller
{
    public function send()
    {
        return Inertia::render('Transaction/Send');
    }

    public function store()
    {
        Request::validate([
            'amount' => ['required', 'integer', 'min:10000', 'max:2000000'],
            'email' => ['required', 'email'],
            'description' => ['nullable', 'string', 'max:100'],
        ]);

        try {
            DB::beginTransaction();

            if (auth()->user()->balance < Request::get('amount')) {
                return Redirect::back()->with('error', 'Insufficient balance.');
            }

            $receiver = User::where('email', Request::get('email'))->first();

            if (!$receiver) {
                return Redirect::back()->with('error', 'Receiver not found.');
            }

            if ($receiver->id === auth()->id()) {
                return Redirect::back()->with('error', 'Cannot send money to yourself.');
            }

            $transfer = Transfer::create([
                'sender_id' => auth()->id(),
                'receiver_id' => auth()->id(),
            ]);

            $transactionSender = new Transaction();

            $transactionSender->user_id = auth()->id();
            $transactionSender->amount = Request::get('amount') * -1;
            $transactionSender->code = 'TRS'. time();
            $transactionSender->status = TransactionStatus::SUCCESS;
            $transactionSender->description = Request::get('description');

            $transactionSender->transactionable()->associate($transfer);
            $transactionSender->save();

            $transactionReceiver = new Transaction();

            $transactionReceiver->user_id = $receiver->id;
            $transactionReceiver->amount = Request::get('amount');
            $transactionReceiver->code = 'TRR'. (time() + 1);
            $transactionReceiver->status = TransactionStatus::SUCCESS;
            $transactionReceiver->description = Request::get('description');

            $transactionReceiver->transactionable()->associate($transfer);
            $transactionReceiver->save();

            $receiver->increment('balance', Request::get('amount'));
            Auth::user()->decrement('balance', Request::get('amount'));

            DB::commit();
            return Redirect::route('home')->with('success', 'Money sent successfully!');
        } catch (\Exception $error) {
            DB::rollBack();
            return Redirect::route('home')->with('error', $error->getMessage());
        }
    }
}
