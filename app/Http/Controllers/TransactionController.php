<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class TransactionController extends Controller
{
    public function index()
    {
        return Inertia::render('Transaction/History', [
            'filters' => Request::all('search', 'trashed'),
            'transactions' => Auth::user()->transactions()
                ->with('transactionable')
                ->orderBy('created_at', 'desc')
                ->filter(Request::only('search', 'trashed'))
                ->paginate(10)
                ->withQueryString()
                ->through(fn ($organization) => [
                    'id' => $organization->id,
                    'code' => $organization->code,
                    'amount' => $organization->amount,
                    'status' => $organization->status,
                    'description' => $organization->description,
                    'receipt_path' => $organization->receipt_path,
                    'type' => $organization->transactionable_type,
                    'transactionable' => $organization->transactionable,
                    'created_at' => $organization->created_at,
                ]),
        ]);
    }
}
