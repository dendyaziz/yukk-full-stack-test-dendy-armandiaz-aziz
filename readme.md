# Fuluz

Transforming Transactions, Redefining Confidence

## Prerequisite

- PHP 8.1

## Installation

Clone the repo locally:

```sh
git clone https://gitlab.com/dendyaziz/yukk-full-stack-test-dendy-armandiaz-aziz fuluz
cd fuluz
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm ci
```

Build assets:

```sh
npm run dev
```

Setup configuration:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

Create an SQLite database. You can also use another database (MySQL, Postgres), simply update your configuration accordingly.

```sh
touch database/database.sqlite
```

Run database migrations:

```sh
php artisan migrate
```

Run database seeder:

```sh
php artisan db:seed
```

Run the dev server (the output will give the address):

```sh
php artisan serve
```

We are ready to go! Visit Fuluz in your browser, and login with:

- **Username:** johndoe@yukk.me
- **Password:** Password!

