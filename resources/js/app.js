import {createApp, h} from 'vue'
import {InertiaProgress} from '@inertiajs/progress'
import {createInertiaApp} from '@inertiajs/inertia-vue3'

InertiaProgress.init()

createInertiaApp({
  resolve: name => require(`./Pages/${name}`),
  title: title => title ? `${title} - Fuluz` : 'Fuluz',
  setup({el, App, props, plugin}) {
    const app = createApp({render: () => h(App, props)})
      .use(plugin)

    app.config.globalProperties.$filters = {
      number(value) {
        const userLocale = navigator.language || 'en-US';
        return Number(value).toLocaleString(userLocale)
      },
      date(value) {
        const dateObject = new Date(value);
        const userLocale = navigator.language || 'en-US';
        const options = {day: 'numeric', month: 'long', year: 'numeric'};

        return dateObject.toLocaleDateString(userLocale, options);
      },
    }

    app.mount(el)
    return app
  },
})
